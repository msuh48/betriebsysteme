#!/usr/bin/env python3

import os
import json
import fnmatch
import subprocess
from distutils import dir_util, file_util
import tempfile
import sys
try:
    import requests
except ImportError:
    print("The module 'requests' is required to upload submissions.")
    print("Please make sure it is correctly installed, before running this script again.")
    exit(1)


def validate_name(inp):
    return inp


def validate_matrikelnummer(inp):
    if not inp.isnumeric():
        return None
    return inp

def validate_yes_no(default=True):
    def boolean_validator(inp):
        if len(inp) == 0:
            return default
        if inp in ["Y", "y"]:
            return True
        if inp in ["N", "n"]:
            return False
        return None
    return boolean_validator


def list_validator(acceptable_inputs):
    def list_or_numeric_validator(inp):
        if inp.isnumeric():
            index = int(inp) - 1
            return acceptable_inputs[index] if 0 <= index < len(acceptable_inputs) else None
        return inp if inp in acceptable_inputs else None
    return list_or_numeric_validator


def default_validator(default):
    return lambda inp: default if inp == "" else inp


def prompt_list(options, description):
    for idx, option in enumerate(options):
        print(" %d\t%s" % (idx + 1, option))
    return prompt(description, list_validator(options))


def prompt(description, validator=validate_name):
    while True:
        inp = validator(input(description))
        if inp is None:
            print("Sorry, again.")
            continue
        return inp


class AssignmentVariant:
    def __init__(self, parent, config_json):
        self.parent = parent
        self.root_dir = config_json["root"]
        self.build_command = config_json["build"]
        self.allowed_file_patterns = config_json["allowed_files"]

    def is_file_allowed(self, filename):
        for pattern in self.allowed_file_patterns:
            if fnmatch.fnmatch(filename, pattern):
                return True
        return False

    def copy_submission(self, dst):
        if not os.path.isdir(self.root_dir):
            raise IOError("cannot copy submission. '%s' is not a directory" % self.root_dir)

        names = os.listdir(self.root_dir)

        dst = os.path.join(dst, self.root_dir)
        dir_util.mkpath(dst)

        for n in names:
            src_name = os.path.join(self.root_dir, n)
            dst_name = os.path.join(dst, n)

            if os.path.isdir(src_name) and not os.path.basename(n) in [".git", ".idea"]:
                dir_util.mkpath(dst_name)
                names.extend([os.path.join(n, child) for child in os.listdir(src_name)])
            else:
                if self.is_file_allowed(n):
                    file_util.copy_file(src_name, dst_name)


class Assignment:
    def __init__(self, json_config):
        self.id: str = json_config["id"]
        self.allowed_members: int = json_config["team_members"]
        if "variants" not in json_config:
            self.variants = [AssignmentVariant(self, json_config)]
        else:
            self.variants = [AssignmentVariant(self, var) for var in json_config["variants"]]


def create_new_teamfile(teamfile, max_members):
    members = []
    print("%s needs to be created." % teamfile)
    print()

    while True:
        print("Please enter details of team member %d." % (len(members) + 1))

        lastname = prompt("Last Name: ", validate_name)
        firstname = prompt("First Name: ", validate_name)
        matrikelnummer = prompt("Matrikelnummer: ", validate_matrikelnummer)

        member = (lastname, firstname, matrikelnummer)
        print()
        print("%s, %s (%s)" % member)
        if not prompt("Is this correct? [y/N] ", validate_yes_no(False)):
            continue
        members.append(member)

        if len(members) >= max_members or prompt("Another? [Y/n] ", validate_yes_no(True)) is False:
            break

    with open(teamfile, "w") as f:
        for member in members:
            f.write("%s, %s, %s\n" % (member[0], member[1], member[2]))
    print("%s successfully created" % teamfile)


def run_command(command, log, cwd=None, error_message=""):
    if cwd is not None:
        result = subprocess.call(command, shell=True, cwd=cwd, stdout=log, stderr=log)
    else:
        result = subprocess.call(command, shell=True, stdout=log, stderr=log)
    if result != 0:
        print(error_message)
        exit(result)


def get_tar(log):
    is_mac = sys.platform == 'darwin'
    tarcmd = "gtar" if is_mac else "tar"
    log.write("=== Checking for %s on %s ===\n" % (tarcmd, sys.platform))
    try:
        tar_fine = 0 == subprocess.call([tarcmd, "--version"], stdout=log, stderr=log)
    except FileNotFoundError:
        tar_fine = False

    if not tar_fine:
        print("It seems like %s is not installed on your system." % tarcmd)
        if is_mac:
            print("GNU tar can be installed using Homebrew: brew install gnu-tar")
        return None
    return tarcmd


def pack_archive(tar_cmd, submission, tmpdir, archive_file, log):
    tar_args = [tar_cmd, "cvzf", os.path.abspath(archive_file),
                "team.txt", submission.root_dir]
    result = subprocess.call(tar_args, stdout=log, stderr=log, cwd=tmpdir)
    if result != 0:
        print("Failed to pack archive.")
        exit(result)


def main():
    with open('.submissionconfig.json') as jsonfile:
        config = json.load(jsonfile)

    assignments = [Assignment(asg) for asg in config["assignments"]]
    selected_assignment = assignments[0] if len(assignments) == 1 else None
    while selected_assignment is None:
        assignment_names = [ass.id for ass in assignments]
        selected_assignment_name = prompt_list(assignment_names, "Select an assignment to submit: ")
        selected_assignment = next(ass for ass in assignments if ass.id == selected_assignment_name)

    possible_variants = [variant for variant in selected_assignment.variants if os.path.exists(variant.root_dir)]
    if len(possible_variants) == 0:
        print("No files for the chosen assignment can be found.")
        print("Allowed files are:")
        for variant in selected_assignment.variants:
            print(" - " + variant.root_dir)
        exit(2)

    submission = possible_variants[0] if len(possible_variants) == 1 else None
    while submission is None:
        submission_paths = [variant.root_dir for variant in possible_variants]
        selected_path = prompt_list(submission_paths, "Select which files you want to submit: ")
        submission = next(variant for variant in possible_variants if variant.root_dir == selected_path)

    print("Selected submission for assignment '%s'" % submission.parent.id)

    teamfile = os.path.join(submission.root_dir, "team.txt")
    archive_file = config.get("archive_file") or "archive.tar.gz"
    build_log_file = config.get("build_log_file") or "build.log"
    upload_log_file = config.get("upload_log_file") or "upload.log"
    evaluation_log_file = config.get("evaluation_log_file") or "evaluation.log"

    create_teamfile = True
    if os.path.isfile(teamfile):
        # If teamfile exists, ask if it should be replaced.
        create_teamfile = not prompt("%s already exists. Do you want to keep it? [Y/n] " % teamfile,
                                     validate_yes_no(True))
    if create_teamfile:
        create_new_teamfile(teamfile, submission.parent.allowed_members)

    with open(build_log_file, "w", buffering=1) as build_log:
        tar_cmd = get_tar(build_log)
        if tar_cmd is None:
            exit(1)

        # Create temporary directory to build submission.
        with tempfile.TemporaryDirectory() as tmpdir:
            submission.copy_submission(tmpdir)
            submission_cwd = os.path.join(tmpdir, submission.root_dir)

            print("Building submission using '%s'" % submission.build_command)
            build_log.write("=== Starting Build: '%s' ===\n" % submission.build_command)
            run_command(submission.build_command, build_log, submission_cwd,
                        "Failed to build submission. A log has been written to " + build_log_file)

        # Create clean temporary directory to pack archive.
        with tempfile.TemporaryDirectory() as tmpdir:
            submission.copy_submission(tmpdir)
            file_util.copy_file(teamfile, os.path.join(tmpdir, "team.txt"))

            print("Packing submission for upload.")
            build_log.write("=== Creating Archive ===\n")
            pack_archive(tar_cmd, submission, tmpdir, archive_file, build_log)
            print("Packed submission is available as " + archive_file)

    server = config["server"]
    host = prompt("Where should the submission be uploaded to (Default '%s')? " % server["hostname"],
                  default_validator(server["hostname"]))
    if not host.startswith("http"):
        host = "http://" + host

    # Exit context of temporary file. Archive is packed in the current directory.
    with open(upload_log_file, "w", buffering=1) as upload_log:
        upload_route = "/api/courses/%s/assignments/%s/submit" % (config["course"], selected_assignment.id)

        upload_log.write("Upload Host:  %s\n" % host)
        upload_log.write("Upload Route: %s\n" % upload_route)
        upload_log.write("=== Starting Upload ===\n")
        with open(archive_file, "rb") as archive_stream:
            upload = {"archive": (archive_file, archive_stream)}
            try:
                response = requests.post("%s:%s%s" % (host, server["port"], upload_route), files=upload)
                upload_log.write("Upload completed: %s %s\n" % (response.status_code, response.reason))
                upload_log.write(response.text)
            except requests.exceptions.ConnectionError as e:
                print("Upload failed: " + str(e))
                upload_log.write(str(e))
                exit(1)

        if not response.ok:
            print(response.text)
            print("Upload failed: " + str(response.status_code))
            exit(1)

        print("Upload complete.")
        upload_response = json.loads(response.text)
        upload_log.write(upload_response["log"])
        if not upload_response.get("accepted"):
            print("Your submission was rejected. A log has been written to " + upload_log_file)
        submission_id = upload_response["submission_id"]
        if "evaluation" in config and  config["evaluation"] is False:
            print("Done! Your Submission-ID: %s\n" %submission_id)
            exit(0)

    with open(evaluation_log_file, "w", buffering=1) as evaluation_log:
        result_route = '/api/results/%s' % submission_id

        log_id = "Submission-ID: %s\n" % submission_id
        log_host = "Fetching result from %s:%s%s\n" % (host, server["port"], result_route)

        evaluation_log.write(log_id)
        evaluation_log.write(log_host)
        print(log_id, end="")
        print(log_host, end="")

        print("Fetching result", end="")
        import time
        while True:
            print(".", end="", flush=True)
            result_response = requests.get("%s:%s%s" % (host, server["port"], result_route))

            if result_response.status_code == 428:
                # Submission is still under evaluation. Wait before trying again.
                time.sleep(1)
                continue

            else:
                print()
                print(result_response.text)

                if not result_response.ok:
                    print("Failed to fetch result: %s" % result_response.status_code)

                exit(not result_response.ok)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print()
        print("Aborted by user.")
    except KeyError or json.decoder.JSONDecodeError as e:
        print("Failed to load configuration file. Is it corrupted?")
