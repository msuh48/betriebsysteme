#include "vmm.h"
#include "output_utility.h"

#include <stdio.h>

char* pagetable;
/**
 * Initialized a statistics object to zero.
 * @param stats A pointer to an uninitialized statistics object.
 */
static void statistics_initialize(Statistics *stats) {
    stats->tlb_hits = 0;
    stats->pagetable_hits = 0;
    stats->total_memory_accesses = 0;
}

Statistics simulate_virtual_memory_accesses(FILE *fd_addresses, FILE *fd_backing) {
    // Initialize statistics
    Statistics stats;
    statistics_initialize(&stats);

    // TODO: Implement your solution
    char *line;
    int len;
    int address;
    create_pagetable();
    while(read = getline(&line, &len, fp) != -1){
      address = atoi(line);
      get_physical_address(address);
    }
    return stats;
}


int get_physical_address(int address){
    int page_number = (address & 0x0000FF00) >> 8;
    int offset = (address & 0x000000FF);
    printf("%d,%d\n", page_number,offset);
    int page = pagetable[page_number];
    int paddress = (page << 8) | offset;
    return paddress;
}

void create_pagetable(){
  int size = 256;
  pagetable = malloc(size);
}
