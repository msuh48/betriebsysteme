#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include "command.h"
#include "statuslist.h"

char * head = "PID\tPGID\tSTATUS";
StatusList * statusList = NULL;


void printStatusList(){
  int i = 0;
  char status[30];
  if(statusList == NULL){
    return;
  }
  List * lst = statusList->statuslist;
    printf("%s\t\tPROG\n",head);
  while(i < statusList->status_list_len){
    StatusElement * elem = lst->head;
    strcpy(status, "");
    if(elem == NULL) {
    } else if(elem->status == -1) {
      strcat(status, "running");
    } else if(WIFEXITED(elem->status) || WIFSIGNALED(elem->status) || WIFSTOPPED(elem->status)){
      sprintf(status, "%s(%d)", term_sig(elem->status), exit_value(elem->status));
    } else {
      strcat(status,"??");
    }
    printf("%d\t%d\t%s\t\t%s\n",elem->pid,elem->pgid,status, elem->prog);
    lst=lst->tail;
    i++;
  }
  clean_Status_List();
}

StatusList * newStatusList(){
  if(statusList == NULL){
    statusList = malloc(sizeof(struct StatusList));
    statusList->statuslist = malloc(sizeof(List));
    statusList->status_list_len = 0;
  }
  return statusList;
}

StatusElement * new_StatusElement(pid_t pid, pid_t pgid, int status, char * prog){
  StatusElement * element = malloc(sizeof(StatusElement));
  element->pid = pid;
  element->pgid = pgid;
  element->status = status;
  strcpy(element->prog,prog);
  statusList->statuslist = list_append(element,statusList->statuslist);
  statusList->status_list_len++;
  return element;
}

void change_status(StatusElement * element,int status){
  element->status = status;
}

void change_status_pid(int pid, int status){
  int i =0;
  List * lst = statusList->statuslist;
  while(i < statusList->status_list_len){
    StatusElement * elem = lst->head;
    if(elem->pid == pid){
      change_status(elem,status);
      break;
    }
    i++;
    lst = lst->tail;
  }
}

/*
void change_status(pid_t pid,int status){
  int i;
  List * lst = statusList->statuslist;
  while(i < statusList->status_list_len){
    StatusElement * elem = lst->head;
    if(elem->pid == pid) {
      elem->status = status;
      return;
    }
    lst = lst->tail;
    i++;
  }
}
*/

void clean_Status_List(){
  //printf("clean_status_list()\n");
  int i = 0;
  if(statusList == NULL){
    return;
  }
  List * current = statusList->statuslist;
  List * before = NULL;
  while(i < statusList->status_list_len){
  // while(current != NULL){
    StatusElement * elem = current->head;
    // printf("%d) (current->head == NULL) = %d\n", i, current->head == NULL);
    if(WIFEXITED(elem->status) || WIFSIGNALED(elem->status)){
      //printf("%d) elem = %s\n", i, elem->prog);
      //waitpid(elem->pid,NULL,WNOHANG);
      if(i == 0) {        // Erstes Element hat keinen Vorgänger
        if(current->tail != NULL) {
          statusList->statuslist = current->tail;
        } else {       // Kein Nachfolger -> Liste kann geleert werden
          statusList->statuslist = NULL;
          return;
        }
      } else {            // Current hat einen Vorgänger
        before->tail = current->tail;
      }
      i--;
      statusList->status_list_len--;
    }
    before = current;
    current = current->tail;
    i++;
  }
}

char* term_sig(int status){
  if(WIFEXITED(status)){
    return "exit";
  }
  if(WIFSIGNALED(status)){
    return "signal";
  }
  if(WIFSTOPPED(status)){
    return "stopped";
  }
  else{
    return "running";
  }
}

int exit_value(int status){
  if(WIFSTOPPED(status)){
    return WEXITSTATUS(status);
  }
  if(WIFEXITED(status)){
    return WEXITSTATUS(status);
  }
  if(WIFSIGNALED(status)){
    return WTERMSIG(status);
  }
  return status;
}
