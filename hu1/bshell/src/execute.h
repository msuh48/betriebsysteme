#ifndef EXECUTE_H
#define EXECUTE_H
#define READ_END 0
#define WRITE_END 1

int execute(Command *);
int numberOfPipes(Command *);
void signal_handler(int sig, siginfo_t *info, void *context);

#endif /* EXECUTE_H */
