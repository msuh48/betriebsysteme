#ifndef STATUSLIST_H

#define STATUSLIST_H

typedef struct StatusElement{
  pid_t pid;
  pid_t pgid;
  int status;
  char prog[9];
} StatusElement;

typedef struct StatusList{
  List * statuslist;
  int status_list_len;
} StatusList;

void printStatusList();
StatusElement * new_StatusElement(int pid, int pgid, int status, char * prog);
void change_status(StatusElement * element,int status);
void change_status_pid(int pid, int status);
int exit_value(int status);
StatusList * newStatusList();
void clean_Status_List();
char * term_sig(int status);
#endif /* end of include guard: STATUSLIST_H */
