#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include "shell.h"
#include "helper.h"
#include "command.h"
#include <signal.h>
#include <errno.h>
#include <pwd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "statuslist.h"
#include "debug.h"
#include "execute.h"

#define READ_END 0
#define WRITE_END 1

/* do not modify this */
#ifndef NOLIBREADLINE
#include <readline/history.h>
#endif /* NOLIBREADLINE */

extern int shell_pid;
extern int fdtty;
StatusList * statusList;
StatusElement * statusElement;
static volatile int signalPid = -1;

/* do not modify this */
#ifndef NOLIBREADLINE
static int builtin_hist(char ** command){

    register HIST_ENTRY **the_list;
    register int i;
    printf("--- History --- \n");

    the_list = history_list ();
    if (the_list)
        for (i = 0; the_list[i]; i++)
            printf ("%d: %s\n", i + history_base, the_list[i]->line);
    else {
        printf("history could not be found!\n");
    }

    printf("--------------- \n");
    return 0;
}
#endif /*NOLIBREADLINE*/
void unquote(char * s){
	char * buffer = calloc(sizeof(char), strlen(s));
	if (s!=NULL){
		if (s[0] == '"' && s[strlen(s)-1] == '"'){
			strcpy(buffer, s);
			strncpy(s, buffer+1, strlen(buffer)-2);
                        s[strlen(s)-2]='\0';
			free(buffer);
		}
	}
}

void unquote_command_tokens(char ** tokens){
    int i=0;
    while(tokens[i] != NULL) {
        unquote(tokens[i]);
        i++;
    }
}

static int execute_fork(SimpleCommand *cmd_s, int background){
    char ** command = cmd_s->command_tokens;
    char * prog_Name = command[0];
    //printf("prog_Name = %s\n", prog_Name);
    unquote_command_tokens(command);
    pid_t pid, wpid;
    Redirection * redirection;
    int fd;

    // https://stackoverflow.com/questions/48830222/get-pid-of-the-signal-sender-in-c
    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = signal_handler;
    sigaction(SIGCHLD, &sa, NULL);
    // signal(SIGCHLD,signal_handler);
    pid = fork();
    if (pid==0){
        /* child */
        signal(SIGINT, SIG_DFL);
        signal(SIGTTOU, SIG_DFL);
        /*
         * handle redirections here
         */
         while(cmd_s->redirections != NULL){
             redirection = cmd_s->redirections->head;
               switch(redirection->r_type){
                   case R_FD:
                     switch(redirection->r_mode){
                       case M_WRITE:
                       // printf("redirection write fd:%i\n",redirection->u.r_fd);
                          dup2(redirection->u.r_fd,1);
                          break;
                       case M_APPEND:
                          dup2(redirection->u.r_fd,1);
                          break;
                       case M_READ:
                         // printf("redirection read fd:%i\n",redirection->u.r_fd);
                         dup2(redirection->u.r_fd,0);
                         break;
                     }
                     break;
                   case R_FILE:
                     switch(redirection->r_mode){
                       case M_WRITE:
                         fd = open(redirection->u.r_file,O_CREAT | O_TRUNC | O_WRONLY,0777);
                         //fcntl(fd, F_SETFD, O_EXCL);
                         if(fd == -1){
                          perror("");
                          printf("Fehler beim Öffnen der Datei: %s\n", redirection->u.r_file);
                          exit(99);
                         }
                         dup2(fd,1);
                         close(fd);
                         break;
                       case M_APPEND:
                         fd = open(redirection->u.r_file,O_CREAT | O_APPEND | O_WRONLY, 0777);
                         if(fd == -1){
                            perror("");
                            printf("Fehler beim Öffnen der Datei: %s\n", redirection->u.r_file);
                            exit(99);
                         }
                         dup2(fd,1);
                         close(fd);
                         break;
                       case M_READ:
                          fd = open(redirection->u.r_file,O_RDONLY);
                          if(fd == -1){
                            perror("");
                            printf("Fehler beim Öffnen der Datei: %s\n", redirection->u.r_file);
                            exit(99);
                           }
                          dup2(fd,0);
                          close(fd);
                         break;
                     }
                     break;
               }
               cmd_s->redirections = cmd_s->redirections->tail;
             }
        if (execvp(command[0], command) == -1){
            fprintf(stderr, "-bshell: %s : command not found \n", command[0]);
            perror("");
        }
        /*exec only return on error*/
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        perror("shell");

    } else {
        /*parent*/
        setpgid(pid, pid);
        if (background == 0) {
            int status = -1;
            int res;

            /* wait only if no background process */
            tcsetpgrp(fdtty, pid);

            /**
             * the handling here is far more complicated than this!
             * vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
             */
            statusElement = new_StatusElement(pid,getpgid(pid),status,prog_Name);
            wpid= waitpid(pid, &status, WUNTRACED | WCONTINUED);
            if(WIFEXITED(status)){
              res = WEXITSTATUS(status);
              change_status_pid(pid,status);
            }else{
              res = 1;
            }
            if(WIFSIGNALED(status)){
              change_status_pid(pid,status);
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            tcsetpgrp(fdtty, shell_pid);
            return res;
        } else {
            //tcsetpgrp(fdtty, pid);
            int status = -1;
            int res;
            fprintf(stderr,"PID=%d PGID=%d Status=%d PROG=%s \n", pid, getpgid(pid),status,prog_Name);
            statusElement = new_StatusElement(pid,getpgid(pid),status,prog_Name);
        }
    }

    return 0;
}


static int do_execute_simple(SimpleCommand *cmd_s, int background){
    if (cmd_s==NULL){
        return 0;
    }
    if (strcmp(cmd_s->command_tokens[0],"exit")==0){
        if(cmd_s->command_token_counter > 1){
          if(strcmp(cmd_s->command_tokens[1],"0")==0){
            exit(0);
          }
          if(strcmp(cmd_s->command_tokens[1],"1")==0){
            exit(1);
          }
        }else{
          exit(0);
        }
    } else if (strcmp(cmd_s->command_tokens[0],"cd")==0){
      if(cmd_s->command_token_counter > 1){
        // printf("%s\n", cmd_s->command_tokens[1]);
        if(chdir(cmd_s->command_tokens[1]) == -1){
          fprintf(stderr, "%s: %s\n", cmd_s->command_tokens[1], strerror(errno));
        }
        return 0;
      } else{
          chdir(getenv("HOME"));
          return 0;
      }
    } else if(strcmp(cmd_s->command_tokens[0],"status")==0){
        if(statusList == NULL){
          statusList = newStatusList();
        }
        printStatusList();
        return 0;

/* do not modify this */
#ifndef NOLIBREADLINE
    } else if (strcmp(cmd_s->command_tokens[0],"hist")==0){
        return builtin_hist(cmd_s->command_tokens);
#endif /* NOLIBREADLINE */
    } else {
        return execute_fork(cmd_s, background);
    }
    fprintf(stderr, "This should never happen!\n");
    exit(1);
}

/*
 * check if the command is to be executed in back- or foreground.
 *
 * For sequences, the '&' sign of the last command in the
 * sequence is checked.
 *
 * returns:
 *      0 in case of foreground execution
 *      1 in case of background execution
 *
 */
int check_background_execution(Command * cmd){
    List * lst = NULL;
    int background =0;
    switch(cmd->command_type){
    case C_SIMPLE:
        lst = cmd->command_sequence->command_list;
        background = ((SimpleCommand*) lst->head)->background;
        break;
    case C_OR:
    case C_AND:
    case C_PIPE:
    case C_SEQUENCE:
        /*
         * last command in sequence defines whether background or
         * forground execution is specified.
         */
        lst = cmd->command_sequence->command_list;
        while (lst !=NULL){
            background = ((SimpleCommand*) lst->head)->background;
            lst=lst->tail;
        }
        break;
    case C_EMPTY:
    default:
        break;
    }
    return background;
}


int execute(Command * cmd){
    int res=0;
    List * lst=NULL;
    int num_pipes = numberOfPipes(cmd);
    int pfd[num_pipes][2];
    pid_t pid[num_pipes + 1];

    int execute_in_background=check_background_execution(cmd);
    switch(cmd->command_type){
    case C_EMPTY:
        break;
    case C_SIMPLE:
        res=do_execute_simple((SimpleCommand*) cmd->command_sequence->command_list->head, execute_in_background);
        fflush(stderr);
        break;
    case C_OR:
     lst = cmd->command_sequence->command_list;
     while (lst !=NULL){
        res = do_execute_simple((SimpleCommand*) lst->head, execute_in_background);
        fflush(stderr);
        if(res == 0){
          lst = NULL;
        }else{
            lst=lst->tail;
          }
      }
      break;
    case C_AND:
     lst = cmd->command_sequence->command_list;
     while (lst !=NULL){

     res=do_execute_simple((SimpleCommand*) lst->head, execute_in_background);
     fflush(stderr);
     if(res != 0){
       break;
     }
       lst=lst->tail;
     }
     break;
    case C_SEQUENCE:
         lst = cmd->command_sequence->command_list;
         while (lst !=NULL){

         res=do_execute_simple((SimpleCommand*) lst->head, execute_in_background);
         fflush(stderr);

         lst=lst->tail;
         }
        break;
    case C_PIPE:
        lst = cmd->command_sequence->command_list;
        // int num_pipes = cmd->command_sequence->command_list_len - 1;


        // 1. for( Pipes erzeugen)
        for(int i = 0; i < num_pipes; i++) {
            pipe(pfd[i]);
            if(fcntl(pfd[i][READ_END], F_SETFD, FD_CLOEXEC) == -1) {
                perror("fcntl pfd[i][READ_END]");
                exit(99);}
            if(fcntl(pfd[i][WRITE_END], F_SETFD, FD_CLOEXEC) == -1) {
                perror("fcntl pfd[i][WRITE_END]");
                exit(99);}
        }

        // 2. for( prozesse, dup2, exec)
        for(int i = 0; i <= num_pipes && lst != NULL; i++) {
            pid[i] = fork();
            char **command = ((SimpleCommand *)lst->head)->command_tokens;
            if (pid[i] == 0) {
                setpgid(0, pid[0]);
                tcsetpgrp(fdtty, getpgid(0));
                signal(SIGINT, SIG_DFL);
                signal(SIGTTOU, SIG_DFL);
                // printf("--- Im Kindprozess (%d) ---\n", getpid());
                // printf("Elternprozess: %d\n", getppid());
                // printf("i = %d, num_pipes = %d, cmd = %s\n", i, num_pipes, command[0]);
                // printf("argv[%d] = %s\n", idx[i], argv[idx[i]]);
                if(i > 0) { dup2(pfd[i - 1][READ_END], 0); }
                if(i < num_pipes) { dup2(pfd[i][WRITE_END], 1); }
                // if(do_execute_simple((SimpleCommand*) lst->head, 0) == -1) {
                execvp(command[0], command);
                perror(command[0]);
                exit(99);
            }
            setpgid(pid[i], pid[0]);
            tcsetpgrp(fdtty, pid[0]);
            statusElement = new_StatusElement(pid[i],getpgid(pid[i]),-1,command[0]);
            lst = lst->tail;
        }

        // 3. for( eltern, pipe-desc schließen)
        for(int i = 0; i < num_pipes; i++) {
            close(pfd[i][READ_END]);
            close(pfd[i][WRITE_END]);
        }
        // 4. for( waitpid auf alle kinder)
        for(int i = 0; i <= num_pipes; i++) {
            int status;
            waitpid(pid[i], &status, WUNTRACED | WCONTINUED);
            if(WIFEXITED(status)){
              res = WEXITSTATUS(status);
              change_status_pid(pid[i],status);
            } else if(WIFSIGNALED(status)){
              change_status_pid(pid[i],status);
            }
        }
        tcsetpgrp(fdtty, shell_pid);
        break;
    default:
        printf("[%s] unhandled command type [%i]\n", __func__, cmd->command_type);
        break;
    }
    return res;
}

int numberOfPipes(Command * cmd){
    int j;
    if(cmd->command_sequence == NULL){
      return 0;
    }
    if(cmd->command_sequence->command_list_len<=2){
      j = 1;
    }
    else{
      j = cmd->command_sequence->command_list_len -1;
    }
    return j;
}
void signal_handler(int sig, siginfo_t *info, void *context){
  //printf("signal %d was send and pid = %d\n",sig, info->si_pid);
  //change_status_pid(info->si_pid,WTERMSIG(sig));
  //psignal(WTERMSIG(sig), "Child term due to");
  int status;
  if(waitpid(info->si_pid, &status, WNOHANG | WUNTRACED)>0){
    change_status_pid(info->si_pid,status);
  }
}
